//
//  Restaurant.swift
//  AqwasTest
//
//  Created by HashEMS on 5/14/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//

import Foundation
import ObjectMapper

struct Restaurant : Mappable {
    var name:String?
    var id:String?
    var rate:String?
    var cateName:String?
    var lat:String?
    var lon:String?
    var error:String?
    init?(map: Map) {

    }
    init(){}
    mutating func mapping(map: Map) {
        error <- map["error"]
        name <- map["name"]
        id <- map["id"]
        rate <- map["rating"]
        cateName <- map["cat"]
        lat <- map["lat"]
        lon <- map["lon"]


    }

}
