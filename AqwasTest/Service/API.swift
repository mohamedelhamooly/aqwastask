//
//  API.swift
//  AqwasTest
//
//  Created by Mohamed Elhamoly 5/14/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//
import Foundation
import Alamofire
import RxSwift
import ObjectMapper
protocol APIServiceProtocol {
    func CallAPIService(funName : String  , type : HTTPMethod   , parameters:Parameters?, H:HTTPHeaders? ) -> Observable<String>
}
class APIService:APIServiceProtocol {
    
    
    func CallAPIService(funName : String  , type : HTTPMethod   , parameters:Parameters?, H:HTTPHeaders? ) -> Observable<String>{
        
        let url = EndPoint.BaseURL + funName
        return Observable.create({ [weak self] (observer) ->  Disposable in
            
            let request =  AF.request(url, method: type, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseString { response in
                switch response.result {
                case .success(let value):
                    if let resCode = response.response?.statusCode ,resCode == 200  || resCode == 201 || resCode == 204
                    {
                        observer.onNext(value)
                        observer.onCompleted()
                    }
                    else
                    {
                        
                        observer.onError(NSError (domain: value, code: -1, userInfo: nil))
                    }
                    
                case .failure(let error):
                    observer.onError(NSError (domain: "domian", code: -1, userInfo: nil))
                    if error._code == NSURLErrorTimedOut {
                        print("Request timeout!")
                    }
                    
                    print(error)
                    
                }
            }
            
            return Disposables.create {
                request.cancel()
            }
        })
        
    }
}
