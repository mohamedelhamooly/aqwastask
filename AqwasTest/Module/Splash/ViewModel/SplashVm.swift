//
//  SplashVm.swift
//  AqwasTest
//
//  Created by Mohamed Elhamoly 5/14/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//

import Foundation
import CoreLocation

class SplashVm {
    lazy var locationManager: CLLocationManager = {
        return CLLocationManager()
    } ()
    lazy var currentLocation: CLLocation = {
        return CLLocation()
    } ()
    
    
    func getInitalLoction() -> CLLocation? {
        locationManager.requestWhenInUseAuthorization()
        if
            CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            currentLocation = locationManager.location!
            return currentLocation
        }
        else {
            return nil
        }
        
    }

}
