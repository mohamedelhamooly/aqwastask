//
//  Details.swift
//  SoftxpertTask
//
//  Created by Mohamed Elhamoly on 2/18/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//

import UIKit
import CoreLocation
class Splash: UIViewController {
    
    var lat_Location:Double?,long_Location:Double?
    lazy var objSplashVm: SplashVm = {
        return SplashVm()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
  }
    
}

extension Splash {
    func setUp()  {
        if let currentLocation  = objSplashVm.getInitalLoction() {
            setInitalLoction(currentLocation)
        }
    }
    func setInitalLoction(_ currentLocation:CLLocation)  {
        lat_Location = currentLocation.coordinate.latitude
        long_Location = currentLocation.coordinate.longitude
    }
}
