//
//  ResturantVM.swift
//  AqwasTest
//
//  Created by Mohamed Elhamoly 5/15/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//
import RxSwift
import RxCocoa
import ObjectMapper
import Foundation
class ResturantVm {
    lazy   var bag:DisposeBag = {
        return DisposeBag()
    }()
    lazy   var showLoading:BehaviorRelay<Bool> = {
        return BehaviorRelay<Bool>(value: true)
    }()
    lazy   var error:BehaviorRelay<String> = {
        return BehaviorRelay<String>(value: "")
    }()
    lazy   var resturantResult:BehaviorRelay<Restaurant> = {
        return BehaviorRelay<Restaurant>(value: Restaurant())
    }()
    
    let apiService: APIServiceProtocol
    init( apiService: APIServiceProtocol = APIService()) {
        self.apiService = apiService
    }
    func loadResturant(Longitude:Double,Latitude:Double ) {
        showLoading.accept(false)
        let parameters = ["uid":"\(Longitude),\(Latitude)",
            "get_param":"value"] as [String : Any]
        apiService.CallAPIService(funName:EndPoint.GetResturant, type: .get, parameters: parameters, H: nil)
            .subscribe(onNext: {[weak self] (jsonResponse) in
                
                let results : Restaurant = Mapper<Restaurant>().map(JSONString: jsonResponse )!
                if results.error == "no" {
                    self?.resturantResult.accept(results)
                    
                }
                else {
                    self?.error.accept(" There was an error Please try again later")
                }
                self?.showLoading.accept(true)
                }
                , onError: { [weak self] (error) in
                    self?.error.accept("Please Check Your Internet")
                    self?.showLoading.accept(true)
                }, onCompleted: {
                    
            }) {
        }.disposed(by: bag)
    }
}
