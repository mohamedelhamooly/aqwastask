//
//  ResturantVc.swift
//  AqwasTest
//
//  Created by Mohamed Elhamoly 5/15/20.
//  Copyright © 2020 mohamed Elhamoly. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ObjectMapper
import GoogleMaps

class ResturantVc: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate {
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblCat: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var googleMaps: GMSMapView!
    private let locationManager = CLLocationManager()
    private var zoom: Float = 20
    private var current_lat: Double = 18.2465
    private var current_lon: Double = 42.5117
    private var currentLocation: CLLocation!
    private lazy var objResturantVm: ResturantVm = {
        return ResturantVm()
    }()
    private lazy var showLoadingDispose: DisposeBag = {
        return DisposeBag()
    } ()
    private lazy var errorDispose: DisposeBag = {
        return DisposeBag()
    } ()
    private lazy var resturantDispose: DisposeBag = {
           return DisposeBag()
       } ()
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    @IBAction func getSuggestClick(_ sender: Any) {
        getResturantData()
    }
    
}
extension ResturantVc {
    // MARK: Maps Business
    func createMap()  {
        let camera = GMSCameraPosition.camera(withLatitude: current_lat, longitude: current_lon, zoom: zoom)
        self.googleMaps.camera = camera
        self.googleMaps.isMyLocationEnabled = true
        self.googleMaps.settings.myLocationButton = true
        self.googleMaps.delegate = self
    }
    func setInitialLocation()  {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        if
            CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() ==  .authorizedAlways
        {
            currentLocation = locationManager.location
        }
        if currentLocation != nil {
            self.current_lat = currentLocation.coordinate.latitude
            self.current_lon =  currentLocation.coordinate.longitude
        }

    }
}
extension ResturantVc {
    // MARK: load & set Data
    func setup()  {
        
        
        objResturantVm.showLoading.asObservable().observeOn(MainScheduler.instance).bind(to: activityIndicator.rx.isHidden).disposed(by: showLoadingDispose)
        objResturantVm.error.asObservable().observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self](message) in
                self?.view.makeToast(message)
            }).disposed(by: errorDispose)
        bindResturantData()
        setInitialLocation()
        createMap()
        getResturantData()
    }
    func bindResturantData()  {
        objResturantVm
            .resturantResult
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self](Resturant) in
                if Resturant.lat != nil {
                    self?.lblCat.text = Resturant.cateName
                    self?.lblName.text = Resturant.name
                    self?.lblRate.text = Resturant.rate
                    self?.current_lat = Double(Resturant.lat!)!
                    self?.current_lon = Double(Resturant.lon!)!
                    self?.UpdateLoction()
                }
                
                
            }).disposed(by: resturantDispose)
    }
    func getResturantData()  {
        objResturantVm.loadResturant(Longitude: current_lat, Latitude: current_lon)
    }
    func UpdateLoction()  {
        let camera = GMSCameraPosition.camera(withLatitude: current_lat, longitude: current_lon, zoom: zoom)
        self.googleMaps.camera = camera
        
    }
}
